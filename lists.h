#ifndef LISTS
#define LISTS

typedef struct node node;
typedef struct list list;

list * listInit();
unsigned int listLength(list *list);
void listAppend(list *list, int value);
void listInsert(list *list, int index, int value);
int listPop(list *list, int index);
char listRemove(list *list, int value);
void listExtend(list *master_list, list *supplementary_list);
void listPrint(list *list);
int listIndex(list *list, int value, int start, int end);
int listCount(list *list, int value);
void listReverse(list *list);
void listClear(list *list);
void listPurge(list *list);
void listSort(list *unsorted_list);

#endif
