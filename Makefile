install: liblists.a lists.h
	install liblists.a /usr/lib/
	install lists.h /usr/include

liblists.a: lists.o
	ar rc liblists.a lists.o
	ranlib liblists.a

lists.o: lists.c
	gcc -c lists.c -o lists.o

uninstall:
	rm /usr/lib/liblists.a
	rm /usr/include/lists.h
