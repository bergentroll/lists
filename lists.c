/*
  lists — pythonic integers lists for C
  Copyright (C) 2018  Anton Karmanov <bergentroll@insiberia.net>

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <stdlib.h>
#include <stdio.h>

/*
  List structure looks like this:

   ____[my_list]____
  |                 |
  node<->node<->node

  Initialize list with:
  list my_list = listInit();

  To free memory use listPurge().
*/

typedef struct node {
  int value;
  struct node *prev, *next;
} node;

typedef struct list {
  unsigned int length;
  node *beginning;
  node *end;
} list;

list * listInit() {
  list *new_list = (list *)malloc(sizeof(list));
  new_list->length = 0;
  new_list->beginning = new_list->end = NULL;
  return new_list;
}

unsigned int listLength(list *list) {
  return list->length;
}


// Add new item to list. Time is N[1].
void listAppend(list *list, int value) {
  node *new_node = (node *)malloc(sizeof(node));
  new_node->value = value;
  new_node->prev = new_node->next = NULL;
  if (!list->end)
    list->beginning = list->end = new_node;
  else {
    new_node->prev = list->end;
    list->end = list->end->next = new_node;
  }
  list->length++;
}

// Insert new item in list in position of index. Time is O[N].
void listInsert(list *list, int index, int value) {
  if (index >= list->length) {
    listAppend(list, value);
  }
  else if (index < 0) {
    fprintf(stderr, "Negative list index.\n");
    exit(1);
  }
  else {
    node *new_node = (node *)malloc(sizeof(node));
    new_node->prev = new_node->next = NULL;
    new_node->value = value;
    node *pointer = list->beginning;
    for (int i = 0; i < index; i++)
      pointer = pointer->next;
    if (pointer->prev) {
      new_node->prev = pointer->prev;
      pointer->prev->next = new_node;
    }
    else
      list->beginning = new_node;
    if (pointer) {
      new_node->next = pointer;
      pointer->prev = new_node;
    }
    else
      list->end = new_node;
    list->length++;
  }
}

/* 
   Pop item from list from index position.
   Use listPop(list, listLength(list)) to pop the last item.
   Time is O[N].
*/
int listPop(list *list, int index) {
  if (list->length == 0 || !list->end) {
    fprintf(stderr, "Trying to pop from empty list.\n");
    exit(1);
  }
  if (index >= list->length)
    index = list->length - 1;
  else if (index < 0)
    index = 0;
  node *pointer;
  int value;
  if (index <= list->length / 2) {
    pointer = list->beginning;
    for (int i = 0; i < index; i++)
      pointer = pointer->next;
  }
  else {
    pointer = list->end;
    for (int i = list->length - 1; i > index; i--)
      pointer = pointer->prev;
  }
  if (list->beginning == pointer)
    list->beginning = pointer->next;
  else
    pointer->prev->next = pointer->next;
  if (list->end == pointer)
    list->end = pointer->prev;
  else
    pointer->next->prev = pointer->prev;
  value = pointer->value;
  free(pointer);
  list->length--;
  return value;
}

// Remove from list first item with value. Time is O[N].
char listRemove(list *list, int value) {
  node *pointer = list->beginning;
  while (pointer) {
    printf("value %i\n", pointer->value);
    if (pointer->value == value) {
      if (pointer->prev)
        pointer->prev->next = pointer->next;
      else {
        if (pointer->next)
          pointer->next->prev = NULL;
        list->beginning = pointer->next;
      }
      if (pointer->next)
        pointer->next->prev = pointer->prev;
      else {
        if (pointer->prev)
          pointer->prev->next = NULL;
        list->end = pointer->prev;
      }
      free(pointer);
      list->length--;
      return 1;
    }
    pointer = pointer->next;
  }
  return 0;
}

/*
  Extend first list with second. Secont will not be changed.
  Time is O[N].
*/
void listExtend(list *master_list, list *supplementary_list) {
  node *pointer = supplementary_list->beginning;
  while (pointer) {
    listAppend(master_list, pointer->value);
    pointer = pointer->next;
  }
}

void listPrint(list *list) {
  node *next_node = list->beginning;
  printf("beginning->");
  while (next_node) {
    printf("%i->", next_node->value);
    next_node = next_node->next;
  }
  puts("end");
}

/*
  Find first item with value and return its index.
  If list does not contain such item, you get -1.
  Time is O[N].
*/
int listIndex(list *list, int value, int start, int end) {
  if (end < 0)
    end = 0;
  else if (end >= list->length)
    end = list->length;
  if (start < 0)
    start = 0;
  else if (start > end)
    start = end;
  node *pointer = list->beginning;
  for (int i = 0; i < end; i++) {
    if (i >= start && pointer->value == value)
      return i;
    pointer = pointer->next;
  }
  return -1;
}

// Count items with value in list. Time is O[N].
int listCount(list *list, int value) {
  int count = 0;
  node *pointer = list->beginning;
  while (pointer) {
    if (pointer->value == value)
      count++;
    pointer = pointer->next;
  }
  return count;
}

// Reverse list order. Time is O[N].
void listReverse(list *list) {
  node *pointer = list->beginning, *next_pointer;
  list->beginning = list->end;
  list->end = pointer;
  while (pointer) {
    next_pointer = pointer->next;
    pointer->next = pointer->prev;
    pointer->prev = next_pointer;
    pointer = next_pointer;
  }
}

// Delete all items from list and free items memory. Time is O[N].
void listClear(list *list) {
  node *next_node, *removing_node = list->beginning;
  while (removing_node) {
    next_node = removing_node->next;
    free(removing_node);
    removing_node = next_node;
  }
  list->beginning = list->end = NULL;
  list->length = 0;
}

// Clear list than free lists memory. It is destroy list.
void listPurge(list *list) {
  listClear(list);
  free(list);
};

// Insertion sort list. Time is O[N^2].
void listSort(list *unsorted_list) {
  list *sorted_list = listInit();
  node *pointer;
  int value;
  char success;
  while (unsorted_list->end) {
    value = listPop(unsorted_list, 0);
    pointer = sorted_list->beginning;
    success = 0;
    for (int i = 0; i < listLength(sorted_list); i++) {
      if (value >= pointer->value) {
        listInsert(sorted_list, i, value);
        success = 1;
        break;
      }
      pointer = pointer->next;
    }
    if (success == 0)
      listInsert(sorted_list, 0, value);
  }
  unsorted_list->beginning = sorted_list->beginning;
  unsorted_list->end = sorted_list->end;
  unsorted_list->length = sorted_list->length;
  free(sorted_list);
}
